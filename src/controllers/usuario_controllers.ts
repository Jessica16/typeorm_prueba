import {Request,Response} from 'express';
import { getRepository } from "typeorm";
import { Usuario } from "../entity/usuario";


export const getUsuarios=async(req:Request,res:Response):Promise<Response>=>{
    const usuarios=await getRepository(Usuario).find();
    return res.json(usuarios);
}

export const crearUsuario=async(req:Request,res:Response):Promise<Response>=>{
    const newUsuario=getRepository(Usuario).create(req.body);
    const results= await getRepository(Usuario).save(newUsuario);
    return res.json(results);
}

export const getUsuario=async(req:Request,res:Response):Promise<Response>=>{
    const results=await getRepository(Usuario).findOne(req.params.id);
    if(results){
        return res.json(results);
    }else{
        return res.status(404).json({msg:"Usuario no registrado en la BD"});
    }
    return res.json(results);
}

export const actualizarUsuario=async(req:Request,res:Response):Promise<Response>=>{
    const usuario=await getRepository(Usuario).findOne(req.params.id);
    if(usuario){
        getRepository(Usuario).merge(usuario,req.body);//combina los datos
        const results= await getRepository(Usuario).save(usuario);//guarda los datos combinados
        return res.json(results);
    }else{
        return res.status(404).json({msg:"Usuario no encontrado"});
    }   
}
export const eliminarUsuario=async(req:Request,res:Response):Promise<Response>=>{
    getRepository(Usuario).delete(req.params.id);
    return res.json({msg:"Usuario eliminado"});
}

