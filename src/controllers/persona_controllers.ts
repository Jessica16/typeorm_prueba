import {Request,Response} from 'express';
import { getRepository } from "typeorm";
import { Persona} from "../entity/persona";
import { Usuario} from "../entity/usuario";

export const getPersonas=async(req:Request,res:Response):Promise<Response>=>{
    const personas=await getRepository(Persona).find();
    return res.json(personas);
}

export const crearPersona=async(req:Request,res:Response):Promise<Response>=>{
    const newPersona=getRepository(Persona).create(req.body);
    const results= await getRepository(Persona).save(newPersona);
    return res.json(results);
}

export const getPersona=async(req:Request,res:Response):Promise<Response>=>{
    const results=await getRepository(Persona).findOne(req.params.id);
    if(results){
        return res.json(results);
    }else{
        return res.status(404).json({msg:"Usuario no registrado en la BD"});
    }
    return res.json(results);
}

export const actualizarPersona=async(req:Request,res:Response):Promise<Response>=>{
    const persona=await getRepository(Persona).findOne(req.params.id);
    if(persona){
        getRepository(Persona).merge(persona,req.body);//combina los datos
        const results= await getRepository(Persona).save(persona);//guarda los datos combinados
        return res.json(results);
    }else{
        return res.status(404).json({msg:"Usuario no encontrado"});
    }   
}
export const eliminarPersona=async(req:Request,res:Response):Promise<Response>=>{
    getRepository(Usuario).delete(req.params.id);
    getRepository(Persona).delete(req.params.id);
    return res.json({msg:"Usuario eliminado"});
}