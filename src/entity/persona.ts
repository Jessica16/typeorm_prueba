import {Entity,Column,PrimaryGeneratedColumn,OneToOne} from 'typeorm';
import { Usuario } from './usuario';

@Entity()
export class Persona{
    @PrimaryGeneratedColumn()
    id:number;

    @Column({
        type:'varchar',
        nullable:false,
        length:50
    })
    nombre_persona:string;

    @Column({
        type:'varchar',
        nullable:false,
        length:50
    })
    apellido_persona:string;

    @Column({
        type:'varchar',
        nullable:false,
        length:10
    })
    cedular:string;

    @OneToOne(type=>Usuario,usuario=>usuario.persona,{
        eager:true,
        cascade:true
    })
    usuario:Usuario;
}