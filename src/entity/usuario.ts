import {Entity,Column,PrimaryGeneratedColumn,OneToOne, JoinColumn} from 'typeorm';
import {Persona} from './persona';

@Entity()
export class Usuario{
    @PrimaryGeneratedColumn()
    id_usuario:number;

    @OneToOne(type=>Persona,persona=>persona.usuario)
    @JoinColumn()
    persona:Persona;

    @Column({
        type:'varchar',
        nullable:false,
        length:30
    })
    correo:string;

    @Column({
        type:'varchar',
        nullable:false,
        length:30
    })
    contrasena:string;
}