import {Router} from 'express';
const router:Router=Router();
import { getUsuarios,crearUsuario,getUsuario,actualizarUsuario,eliminarUsuario} from "../controllers/usuario_controllers";

router.get('/usuarios',getUsuarios);
router.get('/usuarios/:id',getUsuario);
router.post('/usuarios',crearUsuario);
router.put('/usuarios/:id',actualizarUsuario);
router.delete('/usuarios/:id',eliminarUsuario);

export default router;