import {Router} from 'express';
const router:Router=Router();
import { getPersonas,crearPersona,getPersona,actualizarPersona,eliminarPersona} from "../controllers/persona_controllers";
import {eliminarUsuario} from "../controllers/usuario_controllers";
router.get('/personas',getPersonas);
router.get('/personas/:id',getPersona);
router.post('/personas',crearPersona);
router.put('/personas/:id',actualizarPersona);
router.delete('/personas/:id',eliminarPersona);
router.delete('/usuarios/:id',eliminarUsuario);

export default router;