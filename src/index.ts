import 'reflect-metadata';//decoradores @
import express,{ Application} from "express";
import personaRoute from './routes/persona_routes';
import usuarioRoute from './routes/usuario_routes';
import morgan from 'morgan';
import {createConnection} from 'typeorm';

const app:Application=express();
createConnection();

app.set('port',process.env.PORT || 3000);

//middlewars
app.use(morgan('dev'));
app.use(express.json());

//routes
app.use(personaRoute);
app.use(usuarioRoute);


app.listen(app.get('port'));
console.log(`Escuchando al perto: ${app.get('port')}`);
