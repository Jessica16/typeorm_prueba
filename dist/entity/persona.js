"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Persona = void 0;
const typeorm_1 = require("typeorm");
const usuario_1 = require("./usuario");
let Persona = class Persona {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Persona.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({
        type: 'varchar',
        nullable: false,
        length: 50
    }),
    __metadata("design:type", String)
], Persona.prototype, "nombre_persona", void 0);
__decorate([
    typeorm_1.Column({
        type: 'varchar',
        nullable: false,
        length: 50
    }),
    __metadata("design:type", String)
], Persona.prototype, "apellido_persona", void 0);
__decorate([
    typeorm_1.Column({
        type: 'varchar',
        nullable: false,
        length: 10
    }),
    __metadata("design:type", String)
], Persona.prototype, "cedular", void 0);
__decorate([
    typeorm_1.OneToOne(type => usuario_1.Usuario, usuario => usuario.persona, {
        eager: true,
        cascade: true
    }),
    __metadata("design:type", usuario_1.Usuario)
], Persona.prototype, "usuario", void 0);
Persona = __decorate([
    typeorm_1.Entity()
], Persona);
exports.Persona = Persona;
//# sourceMappingURL=persona.js.map