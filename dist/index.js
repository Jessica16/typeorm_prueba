"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata"); //decoradores @
const express_1 = __importDefault(require("express"));
const persona_routes_1 = __importDefault(require("./routes/persona_routes"));
const usuario_routes_1 = __importDefault(require("./routes/usuario_routes"));
const morgan_1 = __importDefault(require("morgan"));
const typeorm_1 = require("typeorm");
const app = express_1.default();
typeorm_1.createConnection();
app.set('port', process.env.PORT || 3000);
//middlewars
app.use(morgan_1.default('dev'));
app.use(express_1.default.json());
//routes
app.use(persona_routes_1.default);
app.use(usuario_routes_1.default);
app.listen(app.get('port'));
console.log(`Escuchando al perto: ${app.get('port')}`);
//# sourceMappingURL=index.js.map