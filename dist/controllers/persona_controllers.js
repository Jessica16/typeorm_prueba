"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.eliminarPersona = exports.actualizarPersona = exports.getPersona = exports.crearPersona = exports.getPersonas = void 0;
const typeorm_1 = require("typeorm");
const persona_1 = require("../entity/persona");
const usuario_1 = require("../entity/usuario");
const getPersonas = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const personas = yield typeorm_1.getRepository(persona_1.Persona).find();
    return res.json(personas);
});
exports.getPersonas = getPersonas;
const crearPersona = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const newPersona = typeorm_1.getRepository(persona_1.Persona).create(req.body);
    const results = yield typeorm_1.getRepository(persona_1.Persona).save(newPersona);
    return res.json(results);
});
exports.crearPersona = crearPersona;
const getPersona = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const results = yield typeorm_1.getRepository(persona_1.Persona).findOne(req.params.id);
    if (results) {
        return res.json(results);
    }
    else {
        return res.status(404).json({ msg: "Usuario no registrado en la BD" });
    }
    return res.json(results);
});
exports.getPersona = getPersona;
const actualizarPersona = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const persona = yield typeorm_1.getRepository(persona_1.Persona).findOne(req.params.id);
    if (persona) {
        typeorm_1.getRepository(persona_1.Persona).merge(persona, req.body); //combina los datos
        const results = yield typeorm_1.getRepository(persona_1.Persona).save(persona); //guarda los datos combinados
        return res.json(results);
    }
    else {
        return res.status(404).json({ msg: "Usuario no encontrado" });
    }
});
exports.actualizarPersona = actualizarPersona;
const eliminarPersona = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    typeorm_1.getRepository(usuario_1.Usuario).delete(req.params.id);
    typeorm_1.getRepository(persona_1.Persona).delete(req.params.id);
    return res.json({ msg: "Usuario eliminado" });
});
exports.eliminarPersona = eliminarPersona;
//# sourceMappingURL=persona_controllers.js.map