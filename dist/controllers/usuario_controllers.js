"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.eliminarUsuario = exports.actualizarUsuario = exports.getUsuario = exports.crearUsuario = exports.getUsuarios = void 0;
const typeorm_1 = require("typeorm");
const usuario_1 = require("../entity/usuario");
const getUsuarios = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const usuarios = yield typeorm_1.getRepository(usuario_1.Usuario).find();
    return res.json(usuarios);
});
exports.getUsuarios = getUsuarios;
const crearUsuario = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const newUsuario = typeorm_1.getRepository(usuario_1.Usuario).create(req.body);
    const results = yield typeorm_1.getRepository(usuario_1.Usuario).save(newUsuario);
    return res.json(results);
});
exports.crearUsuario = crearUsuario;
const getUsuario = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const results = yield typeorm_1.getRepository(usuario_1.Usuario).findOne(req.params.id);
    if (results) {
        return res.json(results);
    }
    else {
        return res.status(404).json({ msg: "Usuario no registrado en la BD" });
    }
    return res.json(results);
});
exports.getUsuario = getUsuario;
const actualizarUsuario = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const usuario = yield typeorm_1.getRepository(usuario_1.Usuario).findOne(req.params.id);
    if (usuario) {
        typeorm_1.getRepository(usuario_1.Usuario).merge(usuario, req.body); //combina los datos
        const results = yield typeorm_1.getRepository(usuario_1.Usuario).save(usuario); //guarda los datos combinados
        return res.json(results);
    }
    else {
        return res.status(404).json({ msg: "Usuario no encontrado" });
    }
});
exports.actualizarUsuario = actualizarUsuario;
const eliminarUsuario = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    typeorm_1.getRepository(usuario_1.Usuario).delete(req.params.id);
    return res.json({ msg: "Usuario eliminado" });
});
exports.eliminarUsuario = eliminarUsuario;
//# sourceMappingURL=usuario_controllers.js.map