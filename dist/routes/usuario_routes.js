"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
const usuario_controllers_1 = require("../controllers/usuario_controllers");
router.get('/usuarios', usuario_controllers_1.getUsuarios);
router.get('/usuarios/:id', usuario_controllers_1.getUsuario);
router.post('/usuarios', usuario_controllers_1.crearUsuario);
router.put('/usuarios/:id', usuario_controllers_1.actualizarUsuario);
router.delete('/usuarios/:id', usuario_controllers_1.eliminarUsuario);
exports.default = router;
//# sourceMappingURL=usuario_routes.js.map