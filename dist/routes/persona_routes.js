"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
const persona_controllers_1 = require("../controllers/persona_controllers");
const usuario_controllers_1 = require("../controllers/usuario_controllers");
router.get('/personas', persona_controllers_1.getPersonas);
router.get('/personas/:id', persona_controllers_1.getPersona);
router.post('/personas', persona_controllers_1.crearPersona);
router.put('/personas/:id', persona_controllers_1.actualizarPersona);
router.delete('/personas/:id', persona_controllers_1.eliminarPersona);
router.delete('/usuarios/:id', usuario_controllers_1.eliminarUsuario);
exports.default = router;
//# sourceMappingURL=persona_routes.js.map